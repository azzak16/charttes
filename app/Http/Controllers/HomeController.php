<?php

namespace App\Http\Controllers;

use App\Source;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['region'] = Source::groupBy('region')->pluck('region')->all();
        $data['branch'] = Source::groupBy('branch')->pluck('branch')->all();
        $data['cluster'] = Source::groupBy('cluster')->pluck('cluster')->all();
        $data['kabupaten'] = Source::groupBy('kabupaten')->pluck('kabupaten')->all();
        $data['site_id'] = Source::groupBy('site_id')->pluck('site_id')->all();
        $data['node'] = Source::groupBy('node')->pluck('node')->all();
        $data['bulan'] = ['01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember'];
        $data['tahun'] = range(1970, 2022);

        return view('home', compact('data'));
    }

    public function chart(Request $request)
    {
        $source = Source::whereYear('trx_date', $request->tahun)
                        ->whereMonth('trx_date', $request->bulan)
                        ->where('region', $request->region)
                        ->where('branch', $request->branch)
                        ->where('cluster', $request->cluster)
                        ->where('kabupaten', $request->kabupaten)
                        ->where('site_id', $request->site_id)
                        ->where('node', $request->node)
                        ->get();

        $monthNum  = $request->bulan;
        $monthName = date('F', mktime(0, 0, 0, $monthNum, 10)); // March


        // With timestamp, this gets last day of April 2010
        $first = date('01-m-Y', strtotime("01-$request->bulan-$request->tahun"));

        $last = date('t-m-Y', strtotime("01-$request->bulan-$request->tahun"));

        $date = range(1, $last);
        $tanggal = [];

        foreach ($date as $value) {
            $tanggal[] = "$value-$request->bulan-$request->tahun";
        }


        $data = [];
        $data['trx_date'] = $tanggal;
        foreach ($source as $value) {
            $data['payload'][] = $value->payload;
            $data['rev_voice'][] = $value->rev_voice;
            $data['rev_bb'][] = $value->rev_bb;
            $data['traffic'][] = $value->traffic;
            $data['revenue'][] = $value->revenue;

        }


        return response()->json([
            'status'     => true,
            'data'  => $data
        ], 200);
    }
}
