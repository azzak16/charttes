@extends('layouts.app')

@section('css')
{{-- <link href="{{asset('adminlte/component/summernote/css/summernote.min.css')}}" rel="stylesheet"> --}}
<style type="text/css">

</style>
@endsection
@section('content')
<form id="form" action="">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label class="form-label" for="tahun">Tahun</label>
                            <div>
                                <select name="tahun" id="tahun" class="form-control">
                                    @foreach ($data['tahun'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="bulan">bulan</label>
                            <div>
                                <select name="bulan" id="bulan" class="form-control">
                                    @foreach ($data['bulan'] as $key=>$item)
                                        <option value="{{$key}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="region">region</label>
                            <div>
                                <select name="region" id="region" class="form-control">
                                    @foreach ($data['region'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="branch">branch</label>
                            <div>
                                <select name="branch" id="branch" class="form-control">
                                    @foreach ($data['branch'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="cluster">cluster</label>
                            <div>
                                <select name="cluster" id="cluster" class="form-control">
                                    @foreach ($data['cluster'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="kabupaten">kabupaten</label>
                            <div>
                                <select name="kabupaten" id="kabupaten" class="form-control">
                                    @foreach ($data['kabupaten'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="site_id">site_id</label>
                            <div>
                                <select name="site_id" id="site_id" class="form-control">
                                    @foreach ($data['site_id'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label" for="node">node</label>
                            <div>
                                <select name="node" id="node" class="form-control">
                                    @foreach ($data['node'] as $item)
                                        <option value="{{$item}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="justify-content-center d-flex">
                        <a href="#" class="btn btn-primary" id="search">Saerch</a>
                    </div>

                    <canvas id="myChart" width="500" height="200"></canvas>

                    <canvas id="myChart2" width="500" height="200"></canvas>

                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection

@push('scripts')
    {{-- <script src="{{ asset('chart.js/chart.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js" integrity="sha512-Lii3WMtgA0C0qmmkdCpsG0Gjr6M0ajRyQRQSbTF6BsrVh/nhZdHpVZ76iMIPvQwz1eoXC3DmAg9K51qT5/dEVg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        var chartData = {};
        var chartDataLine1 = {};
        var chartDataLine2 = {};
        var chartDataLine3 = {};
        var chartDataLine4 = {};

        var chartDataBar = {};
        var chartDataBar1 = {};
        var chartDataBar2 = {};
        var labels = [];

        function respondCanvas() {

            const ctx = document.getElementById('myChart');
            const data = {
                labels: labels,
                datasets: [
                    chartData,
                    chartDataLine1,
                    chartDataLine2,
                    chartDataLine3,
                    chartDataLine4
                ]
            };

            const myChart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Chart.js Combined Line/Bar Chart'
                        }
                    },
                    scales: {
                        y: {
                            type: 'linear',
                            position: 'left',
                            ticks: {
                                color: 'rgba(0,0,0)'
                            },
                            title: {
                                display: true,
                                text: 'Payload'
                            },
                        },
                        y2: {
                            type: 'linear',
                            position: 'right',
                            ticks: {
                                color: 'rgba(0,0,0)'
                            },
                            title: {
                                display: true,
                                text: 'Revenue'
                            },
                        },
                        y3: {
                            type: 'linear',
                            position: 'right',
                            ticks: {
                                color: 'rgba(0,0,0)'
                            },
                            title: {
                                display: true,
                                text: 'Traffic'
                            },
                        }
                    }
                },
            });

        }

        function respondCanvas2() {

            const ctx = document.getElementById('myChart2');
            const data = {
                labels: ['Revenue', 'Payload', 'Traffic', 'Revenue BB', 'Revenue Voice'],
                datasets: [
                    chartDataBar,
                    chartDataBar1,
                    chartDataBar2,
                ]
            };

            const myChart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    indexAxis: 'y',
                    // Elements options apply to all of the options unless overridden in a dataset
                    // In this case, we are setting the border of each horizontal bar to be 2px wide
                    elements: {
                    bar: {
                        borderWidth: 2,
                    }
                    },
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Chart.js Horizontal Bar Chart'
                        }
                    },
                    responsive: true,
                    scales: {
                        x: {
                            stacked: true,
                        },
                        y: {
                            stacked: true
                        }
                    }
                },
            });

        }

        var GetChartData = function () {
            var tahun = $('#tahun').val();
            var bulan = $('#bulan').val();
            var region = $('#region').val();
            var branch = $('#branch').val();
            var cluster = $('#cluster').val();
            var kabupaten = $('#kabupaten').val();
            var site_id = $('#site_id').val();
            var node = $('#node').val();
            $.ajax({
                url: "{{route('chart')}}",
                dataType: 'json',
                method: 'GET',
                data: {
                    tahun: tahun,
                    bulan: bulan,
                    region: region,
                    branch: branch,
                    cluster: cluster,
                    kabupaten: kabupaten,
                    site_id: site_id,
                    node: node,
                },
                // processData: false,
                // contentType: false,
                success: function (d) {
                    chartData = {
                        label: 'Payload',
                        data: d.data.payload,
                        borderColor: 'rgba(107, 155, 232)',
                        backgroundColor: 'rgba(107, 155, 232)',
                        yAxisID: 'y',
                    };

                    chartDataLine2 = {
                        label: 'Revenue Voice',
                        data: d.data.rev_voice,
                        borderColor: 'rgba(240, 225, 17)',
                        backgroundColor: 'rgba(240, 225, 17)',
                        type: 'line',
                        yAxisID: 'y2',
                    };

                    chartDataLine1 = {
                        label: 'Revenue Broadband',
                        data: d.data.rev_bb,
                        borderColor: 'rgba(10, 42, 247)',
                        backgroundColor: 'rgba(10, 42, 247)',
                        type: 'line',
                        yAxisID: 'y2',
                    };

                    chartDataLine3 = {
                        label: 'Traffic',
                        data: d.data.traffic,
                        borderColor: 'rgba(230, 20, 9)',
                        backgroundColor: 'rgba(230, 20, 9)',
                        type: 'line',
                        yAxisID: 'y3',
                    };

                    chartDataLine4 = {
                        label: 'Revenue All',
                        data: d.data.revenue,
                        borderColor: 'rgba(0,0,0)',
                        backgroundColor: 'rgba(0,0,0)',
                        type: 'line',
                        yAxisID: 'y2',
                        fill: true,
                    };

                    chartDataBar = {
                        label: '2G',
                        data: d.data.revenue,
                        borderColor: 'rgba(0,0,0)',
                        backgroundColor: 'rgba(0,0,0)',
                    };

                    chartDataBar1 = {
                        label: '3G',
                        data: d.data.revenue,
                        borderColor: 'rgba(10, 42, 247)',
                        backgroundColor: 'rgba(10, 42, 247)',
                    };

                    chartDataBar2 = {
                        label: '4G',
                        data: d.data.revenue,
                        borderColor: 'rgba(230, 20, 9)',
                        backgroundColor: 'rgba(230, 20, 9)',
                    };

                    labels = d.data.trx_date,

                    respondCanvas();
                    respondCanvas2();
                }
            });
        };

        $(document).ready(function (){

            GetChartData();

            $('#search').on('click', function () {

                GetChartData();
            })

        });

    </script>
@endpush
