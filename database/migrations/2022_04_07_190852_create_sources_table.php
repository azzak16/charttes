<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('trx_date');
            $table->string('lacci_id');
            $table->string('band');
            $table->string('site_id');
            $table->string('node');
            $table->double('long', 15,8);
            $table->double('lat', 15,8);
            $table->string('area');
            $table->string('region');
            $table->string('branch');
            $table->string('cluster');
            $table->string('kabupaten');
            $table->integer('revenue');
            $table->double('payload', 15,8);
            $table->double('traffic', 15,8);
            $table->integer('rev_voice');
            $table->integer('rev_bb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sources');
    }
}
