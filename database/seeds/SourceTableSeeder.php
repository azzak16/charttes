<?php

use App\Source;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->truncate();
        $datas = json_decode(File::get(database_path('datas/source.json')));
        foreach ($datas as $data) {

            $date = str_replace('/', '-', $data->trx_date);

            Source::create([
                'trx_date' 	=> date('Y-m-d', strtotime($date)),
                'lacci_id' 	=> $data->lacci_id,
                'band' 	=> $data->Band,
                'site_id' => $data->site_id,
                'node' => $data->node,
                'long' => $data->long,
                'lat' => $data->lat,
                'area' => $data->area,
                'region' => $data->region,
                'branch' => $data->branch,
                'cluster' => $data->cluster,
                'kabupaten' => $data->kabupaten,
                'revenue' => $data->revenue,
                'payload' => $data->payload,
                'traffic' => $data->traffic,
                'rev_voice' => $data->rev_voice,
                'rev_bb' => $data->rev_bb,
            ]);
        }
    }
}
